# Quad_Ctrl_MAVROS #

This repository contains code of a testing software environment to efficiently implement trajectory tracking 
controllers for multirotors equipped with the [PX4](https://github.com/PX4/PX4-Autopilot) autopilot leveraging on the 
features of ROS and the [MAVROS](https://github.com/mavlink/mavros) package.

At the current version, the testing software environment allows to control two vehicles simultaneously,
while exchanging _messages_ among them. One may extend the functionalities of this software in order to
support a greater number of vehicles, however, bear in mind that further code optimisation may be required.

### Main features ###

* Load reference trajectories from CSV files
* Send failsafe commands from an offboard computer in emergency circumstances
* Set a flying safe zone
* Synchronise two vehicles in order to start tracking a trajectory simultaneously
* Log flight data in rosbags and make plots and animations using the MATLAB functions provided

### Running SITL Simulations in Gazebo ###

* At your ROS workspace, clone the PX4 autopilot repository onto your 'src' folder
using the following command:
```
git clone https://github.com/PX4/PX4-Autopilot.git --recursive
```

* Follow the [installation instructions](https://github.com/mavlink/mavros/blob/master/mavros/README.md#installation) of the MAVROS package

* Setup your SITL environment, the objective is to launch Gazebo with ROS wrappers. Follow the instructions on [this](https://docs.px4.io/master/en/simulation/ros_interface.html) page

* Use the provided launch files to test your controller (an example of a tracking controller is provided in src/main.cpp)

### Experimental Tests ###

This package has been successfully tested in an indoor flying arena featuring a motion capture system, to obtain information regarding the pose of two vehicles which were involved in the experiments.